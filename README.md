# Human Mind

This project is for tracking known bugs in human psychology.

There is, obviously, no code, only issues.

Feel free to put in an issue.

Since we can’t patch the human mind, issues can’t be closed as fixed. Worked-around issues remain open.